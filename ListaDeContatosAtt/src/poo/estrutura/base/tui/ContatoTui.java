package poo.estrutura.base.tui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.swing.JOptionPane;

import poo.estrutura.base.business.ContatoBO;
import poo.estrutura.base.domain.Contato;
import poo.estrutura.base.enums.TipoContatoEnum;

public class ContatoTui {
	private ContatoBO contatoBO = new ContatoBO();
	private Scanner sc = new Scanner(System.in);
	public SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	public void include() {
		System.out.println("INCLUS�O DE CONTATOS\n");
		getContato();

	}

	public void listar() {
		System.out.println("LISTAGEM DE CONTATOS\n");
		listagem();
	}

	public void excluir() {
		System.out.println("REMO��O DE CONTATO\n");
		retirar();
	}

	public void pesquisar() {
		System.out.println("PESQUISA DE CONTATOS");
		find();
	}

	private Date getData(String txt) {

		while (true) {
			try {
				System.out.println(txt);
				String dataString = sc.nextLine();
				Date data = sdf.parse(dataString);
				return data;
			} catch (Exception e) {
				System.out.println("Informe a data novamente: ");
			}
		}

	}

	private String getString(String txt) {
		while (true) {
			System.out.println(txt);
			String texto = sc.nextLine();
			try {
				contatoBO.validarTextoObrigatorio(texto, txt);
				return texto;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	private Integer getInteiro(String txt) {
		System.out.println(txt);
		Integer numero = sc.nextInt();
		sc.nextLine();
		return numero;

	}

	private TipoContatoEnum getTipo(String txt) {
		while (true) {
			System.out.println(txt);
			String tipoString = sc.nextLine();
			try {
				TipoContatoEnum tipoContato = TipoContatoEnum.valueOf(tipoString.toUpperCase());
				return tipoContato;
			} catch (IllegalArgumentException e) {
				System.out.println("Tipo informado n�o v�lido.");
			}
		}
	}

	private void getContato() {
		List<Contato> contatos = new ArrayList<>();

		String nome = getString("Informe o nome: ");
		String telefone = getString("Informe o n�mero: ");
		Integer anoNasc = getInteiro("Informe o ano de nascimento: ");
		Date dataNascimento = getData("Informe a data de nascimento(dia/m�s/ano): ");
		TipoContatoEnum tipo = getTipo("Informe o seu estado(Pessoal/Profissional): ");
		Contato contato = new Contato(nome, telefone, anoNasc, dataNascimento, tipo);

		try {
			contatoBO.include(contato);
		} catch (Exception err) {
			System.out.println(err.getMessage());
		}

	}

	private void listagem() {
		List<Contato> contatos = contatoBO.getAll();

		mostrarSout(contatos);
	}

	private void mostrarSout(List<Contato> contatos) {
		for (Contato contato : contatos) {
			System.out.println(" Contato ");
			System.out.println(" \t" + contato.getNome());
			System.out.println(" \tTelefone=" + contato.getTelefone());
			System.out.println(" \tAno de nascimento=" + contato.getAnoNasc());
			System.out.println(" \tData de nascimento=" + sdf.format(contato.getDataNascimento()));
			System.out.println(" \tEstado do contato=" + contato.getTipo());
		}
	}

	private void retirar() {
		List<Contato> contatos = contatoBO.getAll();
		Integer opc = 0;
		String txt = getString("Informe o contato que deseja remover");
		Contato contato = contatoBO.findByNome(txt);
		if (contato == null) {
			JOptionPane.showMessageDialog(null, "Este contato n�o existe!!!");
		} else {
			opc = JOptionPane.showConfirmDialog(null, "Deseja realmente remover este contato ? " + contato.getNome(),
					"Aviso", JOptionPane.YES_NO_OPTION);
			if (opc == JOptionPane.YES_OPTION) {
				contatos.remove(contato);
			}
		}
	}

	private void retirarOriginal() {
		List<Contato> contatos = contatoBO.getAll();
		Contato contatoRemover = null;
		Integer opc = 0;
		String txt = getString("Informe o contato que deseja remover");
		for (Contato contato : contatos) {
			if (contato.getNome().equalsIgnoreCase(txt)) {
				opc = JOptionPane.showConfirmDialog(null,
						"Deseja realmente remover este contato ? " + contato.getNome(), "Aviso",
						JOptionPane.YES_NO_OPTION);
				if (opc == JOptionPane.YES_OPTION) {
					contatoRemover = contato;

				} else if (opc == JOptionPane.NO_OPTION) {
					break;
				}

			} else {
				JOptionPane.showMessageDialog(null, "Este contato n�o existe!!!");
			}
		}
		if (contatoRemover != null) {
			contatos.remove(contatoRemover);
		}
	}

	private void find() {
		List<Contato> contatos = contatoBO.getAll();
		String nome = getString("Informe o nome do contato que deseja pesquisar");
		String telefone = getString("Informe o telefone do contato que deseja pesquisar");
		Contato contato = contatoBO.find(nome, telefone);
		mostrarSout(contatos);
	}
}