package br.ucsal;

import org.junit.Assert;
import org.junit.Test;

public class ExercicioTest {
	@Test
	public void maiorNumeroTest() {
		//Definir dados a serem ultilizados no teste
		int vet[] = {4,8,7,4,3};
		//Executar o m�todo que ser� executado
		int maiorNumero = Exercicio.encontrarMaiorNumero(vet);
	    //Definir o resultado esperado, a partir do entendimento do problema
		int resultadoEsperado = 8;
		//Comparar o resultado esperado com o obtido
          Assert.assertEquals(resultadoEsperado, maiorNumero);	
	}
}
