package br.ucsal;

import java.util.Scanner;

public class Exercicio {
	
	static void obterNumeros(int vet[]) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Informe 5 n�meros: ");
		for (int i = 0; i < vet.length; i++) {
			vet[i] = sc.nextInt();
		}
	}
	public static int encontrarMaiorNumero(int vet[]){
		int maior = 0;
		for (int i = 0; i < vet.length; i++) {
			if(vet[i] > maior) {
				maior = vet[i];
			}
		}
		return maior;
	}
	static void exibirMaiorNumero(int maior) {
		System.out.println("Dentre os n�meros informados, o maior foi: " + maior);
	}
	public static void main(String[] args) {
		
		int[] vetor = new int[5];
		obterNumeros(vetor);
		encontrarMaiorNumero(vetor);
		exibirMaiorNumero(encontrarMaiorNumero(vetor));
	}
}
