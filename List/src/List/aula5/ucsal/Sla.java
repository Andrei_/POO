package List.aula5.ucsal;

import java.util.ArrayList;
import java.util.List;

public class Sla {

	public static void main(String[] args) {
		List objetos = new ArrayList();
		
		objetos.add("Andrei");
		objetos.add(123);
		
		System.out.println("Quantidade de itens na Lista" + objetos.size());
		System.out.println("Segundo objeto = " + objetos.get(1));
		System.out.println("Existe o objeto 123? " + objetos.contains(123));
		System.out.println("Existe o objeto 321? " + objetos.contains(321));
		objetos.remove("Andrei");
		System.out.println("Objetos = " + objetos);
		objetos.clear();
		System.out.println("Quantidade de itens na Lista" + objetos.size());
		

	}

}
