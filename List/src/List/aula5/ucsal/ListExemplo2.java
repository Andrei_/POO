package List.aula5.ucsal;

import java.util.ArrayList;
import java.util.List;

public class ListExemplo2 {

	public static void main(String[] args) {
		List<String> nomes = new ArrayList<>();
		nomes.add("a0");
		nomes.add("a1");
		nomes.add("a2");
		
		for (String nome : nomes) {
			System.out.println(nome);
		}
	}

}
